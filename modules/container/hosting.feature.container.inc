<?php

/**
 * @file
 * Expose the Container Service feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_container_hosting_feature() {
  $features['container'] = array(
    'title' => t('Container service'),
    'description' => t('Provides a Container service type.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_container',
    'enable' => 'hosting_container_feature_enable_callback',
    'disable' => 'hosting_container_feature_disable_callback',
    // associate with a specific node type.
    //  'node' => 'nodetype',
    'group' => 'experimental'
    );
  return $features;
}
