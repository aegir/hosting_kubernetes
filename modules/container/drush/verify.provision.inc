<?php

function drush_container_provision_verify() {
  if (d()->type == 'server') {
    d()->service('container')->verify();
  }
}
