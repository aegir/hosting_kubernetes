<?php

/**
 * @file
 *   Add containers to the provision service API.
 *
 *  Declares a new service type that matches the same service definition in the
 *  hosting front end.
 */

/**
 * Implements hook_drush_init().
 */
function container_drush_init() {
  container_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function container_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Implements hook_provision_services().
 *
 * Expose the service type this extension defines to provision.
 *
 * @return
 *   An array with the service type the key, and the default implementation the value.
 */
function container_provision_services() {
  container_provision_register_autoload();
  return array('container' => NULL);
}
