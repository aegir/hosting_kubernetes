<?php
/**
 * @file
 * hosting_deployment.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_deployment_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-deployment-field_deployment_application'.
  $field_instances['node-deployment-field_deployment_application'] = array(
    'bundle' => 'deployment',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the application from which to deploy this site.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_deployment_application',
    'label' => 'Application',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-deployment-field_deployment_status'.
  $field_instances['node-deployment-field_deployment_status'] = array(
    'bundle' => 'deployment',
    'default_value' => array(
      0 => array(
        'value' => 'queued',
      ),
    ),
    'deleted' => 0,
    'description' => 'The status of the deployment.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_deployment_status',
    'label' => 'Status',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Application');
  t('Select the application from which to deploy this site.');
  t('Status');
  t('The status of the deployment.');

  return $field_instances;
}
