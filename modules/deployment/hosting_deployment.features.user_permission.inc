<?php
/**
 * @file
 * hosting_deployment.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hosting_deployment_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create deployment content'.
  $permissions['create deployment content'] = array(
    'name' => 'create deployment content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any deployment content'.
  $permissions['delete any deployment content'] = array(
    'name' => 'delete any deployment content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own deployment content'.
  $permissions['delete own deployment content'] = array(
    'name' => 'delete own deployment content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any deployment content'.
  $permissions['edit any deployment content'] = array(
    'name' => 'edit any deployment content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own deployment content'.
  $permissions['edit own deployment content'] = array(
    'name' => 'edit own deployment content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
