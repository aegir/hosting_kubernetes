<?php

/**
 * @file
 *   Add deployments to the provision context API.
 */

/**
 * Implements hook_drush_init().
 */
function deployment_drush_init() {
  deployment_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function deployment_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Implements hook_drush_command().
 */
function deployment_drush_command() {
  $items = array();

  $items['provision-deploy-application'] = array(
    'description' => 'Deploy a containerized application.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  $items['provision-delete-deployment'] = array(
    'description' => 'Delete a deployment of an application.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  $items['provision-backup-deployment'] = array(
    'description' => 'Backup a deployment of an application.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

/**
 * Command callback for `drush provision-deploy-application`.
 */
function drush_deployment_provision_deploy_application() {
  if (d()->type == 'deployment') {
    d()->application->container_server->service('container')->deployApplication();
  }
}

/**
 * Command callback for `drush provision-delete-deployment`.
 */
function drush_deployment_provision_delete_deployment() {
  if (d()->type == 'deployment') {
    d()->application->container_server->service('container')->deleteDeployment();
  }
}

/**
 * Command callback for `drush provision-backup-deployment`.
 */
function drush_deployment_provision_backup_deployment() {
  if (d()->type == 'deployment') {
    $backup = new Provision_DeploymentBackup(d()->name);
    foreach (d()->resources as $resource) {
      $backup->addItemToBackup($resource);
    }
    $backup->save();
  }
}
