<?php

/**
 * @file Provision named context deployment class.
 */


/**
 * Class for the deployment context.
 */
class Provision_Context_deployment extends Provision_Context {
  public $parent_key = 'application';

  static function option_documentation() {
    return array(
      'server' => 'deployment: drush backend server; default @server_master',
      'application' => 'deployment: the application that defines this deployment\'s resources.',
      'replacements' => 'deployment: the deployment\'s replacements for the applications resource configuration templates.',
    );
  }

  function init_deployment() {
    $this->setProperty('server', '');
    $this->setProperty('application', '');
    $this->setProperty('replacements', array());
  }

}

