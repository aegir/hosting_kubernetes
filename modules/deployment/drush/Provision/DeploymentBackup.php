<?php

/**
 * Generates backups for Aegir Deployments.
 */
class Provision_DeploymentBackup extends Provision_Backup {

  /**
   * {@inheritdoc}
   *
   * Override to add resource volumes, not the resources themselves.
   */
  public function addItemToBackup($item) {
    $volumes = $this->getResourceVolumes($item);
    foreach ($volumes as $volume) {
      drush_log('Adding volume to backup: ' . $volume . '...');
      $this->items[] = $volume;
    }
  }

  /**
   * Fetch all volumes associated with a resource.
   *
   * @param type $resource
   *   The resource whose volumes we're fetching.
   * @return array
   *   The list of volumes.
   */
  protected function getResourceVolumes($resource) {
    return array_merge(
      $this->getResourceNonPersistentVolumes($resource),
      $this->getResourcePersistentVolumes($resource)
    );
  }

  protected function getResourceNonPersistentVolumes($resource) {
    // Include regular Volumes here.
    return array();
  }

  /**
   * Fetch a resource's persistent volumes.
   *
   * @param type $resource
   * @return array
   *   The list of volumes.
   */
  protected function getResourcePersistentVolumes($resource) {
    $pod = $this->getPodId($resource);
    $claims = $this->getPersistentVolumeClaims($pod);
    $volumes = array();
    foreach ($claims as $claim) {
      $volumes[] = $this->getVolumeUri($this->getClaimVolume($claim));
    }
    return $volumes;
  }

  /**
   * Get a pod identifier from a resource.
   *
   * @param string $resource
   *   The resource.
   * @return string
   *   The ID of the pod.
   *
   * @todo Figure out a real conversion here.  Hard-coded to get other stuff working.
   */
  protected function getPodId($resource) {
    if ($resource == '/var/aegir/config/server_minikube/kubernetes.d/deployments/MySQLDemo/resources/mysql-deployment.yaml') {
      return 'wordpress-mysql';
    }
  }

  /**
   * Fetch a pod's persistent volume claims.
   *
   * @param type $pod
   *   The pod.
   * @return array
   *   The list of claims.
   */
  protected function getPersistentVolumeClaims($pod) {
    if (empty($pod)) {
      return array();
    }

    $pod_data = d()->application->container_server->service('container')->kubectl('describe pod ' . $pod);
    $claims = array();
    $delimiters = ': ';
    foreach ($pod_data as $line) {
      if (strpos($line, 'ClaimName') !== FALSE) {
        // Throw away the label and get the value.
        strtok($line, $delimiters);
        $claims[] = trim(strtok($delimiters));
      }
    }
    return $claims;
  }

  /**
   * Return a PersistentVolumeClaim's Volume URI.
   *
   * For now, this will return a file system path where the volume is located.
   * Later, we'll support remote URIs & such.
   *
   * @param string $volume
   *   The volume name.
   * @return string
   *   The volume's URI.
   */
  protected function getVolumeUri($volume) {
    $volume_data = d()->application->container_server->service('container')->kubectl('describe persistentvolume ' . $volume);
    $delimiters = ': ';
    foreach ($volume_data as $line) {
      if (strpos($line, 'Path:') !== FALSE) {
        // Throw away the label and get the value.
        strtok($line, $delimiters);
        $uri = trim(strtok($delimiters));
        return $uri;
      }
    }
  }

  /**
   * Fetch a volume claim's volume.
   *
   * @param type $claim
   *   The volume claim name.
   * @return string
   *   The volume name.
   */
  protected function getClaimVolume($claim) {
    $claim_data = d()->application->container_server->service('container')->kubectl('describe persistentvolumeclaim ' . $claim);
    $delimiters = ': ';
    foreach ($claim_data as $line) {
      if (strpos($line, 'Volume') !== FALSE) {
        // Throw away the label and get the value.
        strtok($line, $delimiters);
        $volume = trim(strtok($delimiters));
        return $volume;
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @todo Include copies of resources in case they're changed or deleted later?
   */
  public function save() {
    drush_log('Archiving deployment volumes for ' . d()->name . '...');

    if (empty($sources = implode(' ', $this->getValidBackupItems()))) {
      drush_log('Empty archive: No volumes to back up.  Exitting...');
      return;
    }

    $path = d()->server->backup_path . '/deployments';
    $destination = $path . '/' . trim(d()->name, '@') . '-' . date('o-m-d\TG-i-s') . '.tar.gz';

    drush_shell_exec('mkdir -p %s', $path);
    if (drush_shell_exec('tar zcvf %s %s', $destination, $sources)) {
      drush_log('Successfully archived deployment volumes for ' . d()->name . ' in ' . $destination . '.');
    }
    else {
      drush_set_error('PROVISION_BACKUP_FAILED', dt("Could not back up deployment volumes for %deployment", array(
        '%deployment' => d()->name,
      )));
    }
  }

}
