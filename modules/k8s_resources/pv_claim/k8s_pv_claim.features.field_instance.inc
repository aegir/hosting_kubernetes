<?php
/**
 * @file
 * k8s_pv_claim.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function k8s_pv_claim_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'resource-k8s_pv_claim-field_k8s_persistent_volume'.
  $field_instances['resource-k8s_pv_claim-field_k8s_persistent_volume'] = array(
    'bundle' => 'k8s_pv_claim',
    'deleted' => 0,
    'description' => 'Persistent volume claims require a persistent volume.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_persistent_volume',
    'label' => 'Persistent volume',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Persistent volume');
  t('Persistent volume claims require a persistent volume.');

  return $field_instances;
}
