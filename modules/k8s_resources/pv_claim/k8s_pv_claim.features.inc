<?php
/**
 * @file
 * k8s_pv_claim.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_pv_claim_eck_bundle_info() {
  $items = array(
    'resource_k8s_pv_claim' => array(
      'machine_name' => 'resource_k8s_pv_claim',
      'entity_type' => 'resource',
      'name' => 'k8s_pv_claim',
      'label' => 'Persistent Volume Claim',
      'config' => array(
        'managed_properties' => array(
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
          'title' => 0,
        ),
        'template' => array(
          'resource' => 'apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
  labels:
    app: wordpress
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi
',
        ),
        'replacements' => array(
          0 => array(
            'selector' => 'spec/resources/requests/storage',
            'token' => '[resource:field-k8s-persistent-volume:field-storage-capacity]Gi',
          ),
          1 => array(
            'selector' => 'metadata/labels/app',
            'token' => '[deployment:name]',
          ),
        ),
      ),
    ),
  );
  return $items;
}
