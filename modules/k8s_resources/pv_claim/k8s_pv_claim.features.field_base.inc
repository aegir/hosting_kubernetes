<?php
/**
 * @file
 * k8s_pv_claim.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function k8s_pv_claim_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_k8s_pv_claim'.
  $field_bases['field_k8s_pv_claim'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_k8s_pv_claim',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'behaviors' => array(),
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'k8s_pv_claim' => 'k8s_pv_claim',
        ),
      ),
      'target_type' => 'resource',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
