<?php
/**
 * @file
 * k8s_mysql_deployment.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_mysql_deployment_eck_bundle_info() {
  $items = array(
    'resource_k8s_mysql_deployment' => array(
      'machine_name' => 'resource_k8s_mysql_deployment',
      'entity_type' => 'resource',
      'name' => 'k8s_mysql_deployment',
      'label' => 'MySQL Deployment',
      'config' => array(
        'managed_properties' => array(
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
          'title' => 0,
        ),
        'template' => array(
          'resource' => 'apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: mysql
    spec:
      containers:
      - image: mysql:5.6
        name: mysql
        env:
          # $ kubectl create secret generic mysql-pass --from-file=password.txt
          # make sure password.txt does not have a trailing newline
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password.txt
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim
',
        ),
        'replacements' => array(
          0 => array(
            'selector' => 'metadata/labels/app',
            'token' => '[deployment:name]',
          ),
          1 => array(
            'selector' => 'spec/template/metadata/labels/app',
            'token' => '[deployment:name]',
          ),
          2 => array(
            'selector' => 'spec/template/spec/containers/0/env/0/valueFrom/secretKeyRef/name',
            'token' => '[deployment:name][resource:id]',
          ),
          3 => array(
            'selector' => 'spec/template/spec/containers/0/env/0/valueFrom/secretKeyRef/key',
            'token' => 'password',
          ),
          4 => array(
            'selector' => 'spec/template/spec/volumes/0/name',
            'token' => 'mysql-persistent-storage',
          ),
          5 => array(
            'selector' => 'spec/template/spec/volumes/0/persistentVolumeClaim/claimName',
            'token' => '[deployment:name][resource:id]',
          ),
          6 => array(
            'selector' => 'spec/template/spec/volumes/0/name',
            'token' => '[deployment:name]-mysql-persistent-storage',
          ),
          7 => array(
            'selector' => 'spec/template/spec/containers/0/volumeMounts/0/name',
            'token' => '[deployment:name]-mysql-persistent-storage',
          ),
          8 => array(
            'selector' => 'spec/template/spec/containers/0/name',
            'token' => '[deployment:name]-mysql',
          ),
          9 => array(
            'selector' => 'spec/template/spec/containers/0/image',
            'token' => '[resource:field-k8s-container-image]',
          ),
        ),
      ),
    ),
  );
  return $items;
}
