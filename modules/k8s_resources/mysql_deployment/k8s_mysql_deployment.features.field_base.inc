<?php
/**
 * @file
 * k8s_mysql_deployment.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function k8s_mysql_deployment_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_k8s_container_image'.
  $field_bases['field_k8s_container_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_k8s_container_image',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'mysql:5.6' => 'mysql:5.6',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_mysql_deployment'.
  $field_bases['field_mysql_deployment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_mysql_deployment',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'behaviors' => array(),
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'k8s_mysql_deployment' => 'k8s_mysql_deployment',
        ),
      ),
      'target_type' => 'resource',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
