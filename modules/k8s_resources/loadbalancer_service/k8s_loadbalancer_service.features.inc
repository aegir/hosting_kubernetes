<?php
/**
 * @file
 * k8s_loadbalancer_service.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_loadbalancer_service_eck_bundle_info() {
  $items = array(
    'resource_k8s_loadbalancer_service' => array(
      'machine_name' => 'resource_k8s_loadbalancer_service',
      'entity_type' => 'resource',
      'name' => 'k8s_loadbalancer_service',
      'label' => 'LoadBalancer service',
      'config' => array(
        'managed_properties' => array(
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
          'title' => 0,
        ),
        'template' => array(
          'resource' => 'apiVersion: v1
kind: Service
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: wordpress
    tier: frontend
  type: LoadBalancer
',
        ),
        'replacements' => array(
          0 => array(
            'selector' => 'metadata/labels/app',
            'token' => '[deployment:name]',
          ),
          1 => array(
            'selector' => 'spec/selector/app',
            'token' => '[deployment:name]',
          ),
        ),
      ),
    ),
  );
  return $items;
}
