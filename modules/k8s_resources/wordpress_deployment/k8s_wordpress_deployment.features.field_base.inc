<?php
/**
 * @file
 * k8s_wordpress_deployment.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function k8s_wordpress_deployment_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_k8s_wordpress_image'.
  $field_bases['field_k8s_wordpress_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_k8s_wordpress_image',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'wordpress:4.4-apache' => 'wordpress:4.4-apache',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
