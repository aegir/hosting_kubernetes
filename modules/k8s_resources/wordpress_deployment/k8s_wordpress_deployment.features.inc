<?php
/**
 * @file
 * k8s_wordpress_deployment.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_wordpress_deployment_eck_bundle_info() {
  $items = array(
    'resource_k8s_wordpress_deployment' => array(
      'machine_name' => 'resource_k8s_wordpress_deployment',
      'entity_type' => 'resource',
      'name' => 'k8s_wordpress_deployment',
      'label' => 'WordPress Deployment',
      'config' => array(
        'managed_properties' => array(
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
          'title' => 0,
        ),
        'template' => array(
          'resource' => 'apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: frontend
    spec:
      containers:
      - image: wordpress:4.4-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: wordpress-mysql
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password.txt
        ports:
        - containerPort: 80
          name: wordpress
        volumeMounts:
        - name: wordpress-persistent-storage
          mountPath: /var/www/html
      volumes:
      - name: wordpress-persistent-storage
        persistentVolumeClaim:
          claimName: wp-pv-claim
',
        ),
        'replacements' => array(
          0 => array(
            'selector' => 'metadata/labels/app',
            'token' => '[deployment:name]',
          ),
          1 => array(
            'selector' => 'spec/template/metadata/labels/app',
            'token' => '[deployment:name]',
          ),
          2 => array(
            'selector' => 'spec/template/spec/containers/0/env/1/valueFrom/secretKeyRef/name',
            'token' => '[deployment:name][resource:field-mysql-deployment:id]',
          ),
          3 => array(
            'selector' => 'spec/template/spec/containers/0/env/1/valueFrom/secretKeyRef/key',
            'token' => 'password',
          ),
          4 => array(
            'selector' => 'spec/template/spec/containers/0/name',
            'token' => '[deployment:name]-wordpress',
          ),
          5 => array(
            'selector' => 'spec/template/spec/containers/0/env/0/value',
            'token' => '[deployment:name][resource:field-mysql-deployment:id]',
          ),
          6 => array(
            'selector' => 'spec/template/spec/containers/0/volumeMounts/0/name',
            'token' => '[deployment:name]-wordpress-persistent-storage',
          ),
          7 => array(
            'selector' => 'spec/template/spec/volumes/0/name',
            'token' => '[deployment:name]-wordpress-persistent-storage',
          ),
          8 => array(
            'selector' => 'spec/template/spec/volumes/0/persistentVolumeClaim/claimName',
            'token' => '[deployment:name][resource:id]',
          ),
          9 => array(
            'selector' => 'spec/template/spec/containers/0/image',
            'token' => '[resource:field-k8s-wordpress-image]',
          ),
        ),
      ),
    ),
  );
  return $items;
}
