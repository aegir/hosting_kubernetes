<?php
/**
 * @file
 * k8s_wordpress_deployment.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function k8s_wordpress_deployment_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'resource-k8s_wordpress_deployment-field_k8s_load_balancer'.
  $field_instances['resource-k8s_wordpress_deployment-field_k8s_load_balancer'] = array(
    'bundle' => 'k8s_wordpress_deployment',
    'deleted' => 0,
    'description' => 'WordPress deployments require a LoadBalancer service.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_load_balancer',
    'label' => 'Load Balancer',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'resource-k8s_wordpress_deployment-field_k8s_pv_claim'.
  $field_instances['resource-k8s_wordpress_deployment-field_k8s_pv_claim'] = array(
    'bundle' => 'k8s_wordpress_deployment',
    'deleted' => 0,
    'description' => 'WordPress deployments require a persistent volume claim.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_pv_claim',
    'label' => 'Persistent volume claim',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 0,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'resource-k8s_wordpress_deployment-field_k8s_wordpress_image'.
  $field_instances['resource-k8s_wordpress_deployment-field_k8s_wordpress_image'] = array(
    'bundle' => 'k8s_wordpress_deployment',
    'default_value' => array(
      0 => array(
        'value' => 'wordpress:4.4-apache',
      ),
    ),
    'deleted' => 0,
    'description' => 'Specify the image to use in building WordPress containers.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_wordpress_image',
    'label' => 'WordPress Image',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'resource-k8s_wordpress_deployment-field_mysql_deployment'.
  $field_instances['resource-k8s_wordpress_deployment-field_mysql_deployment'] = array(
    'bundle' => 'k8s_wordpress_deployment',
    'default_value_function' => '',
    'deleted' => 0,
    'description' => 'WordPress deployments require a MySQL deployment.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_mysql_deployment',
    'label' => 'MySQL deployment',
    'required' => 1,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'status' => 0,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Load Balancer');
  t('MySQL deployment');
  t('Persistent volume claim');
  t('Specify the image to use in building WordPress containers.');
  t('WordPress Image');
  t('WordPress deployments require a LoadBalancer service.');
  t('WordPress deployments require a MySQL deployment.');
  t('WordPress deployments require a persistent volume claim.');

  return $field_instances;
}
