<?php

/**
 * @file
 *   Add applications to the provision context API.
 */

/**
 * Implements hook_drush_init().
 */
function application_drush_init() {
  application_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function application_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

