<?php

/**
 * A configuration class to write a deployment's resource config files.
 */
class Provision_Config_Application extends Provision_Config_Resource {

  public $template = FALSE;

  /**
   * Override the parent class, as we don't use a template, and are going to
   * write multiple resource config file templates.
   */
  public function write() {
    foreach ($this->getResources() as $key => $resource) {
      $path = $this->getConfigPath($key, $resource['filename']);
      $contents = $this->unpackYAML($resource['template']);
      provision_file()->file_put_contents($path, $contents)
        ->succeed('Wrote application config file template to @path')
        ->fail('Failed to write application config file template to @path');
    }
    // Sync out to the server.
    // @todo Check whether perms aren't changeable because of minikube.
    $this->getContainerServer()->sync($this->getResourceDir(), array('no-perms' => TRUE));
  }

  function getContext($type = 'application') {
    return parent::getContext($type);
  }

}
