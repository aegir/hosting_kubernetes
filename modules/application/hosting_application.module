<?php
/**
 * @file
 * Code for the Hosting Application feature.
 */

include_once 'hosting_application.features.inc';

/**
 * Implements hook_hosting_tasks().
 */
function hosting_application_hosting_tasks() {
  $tasks = array();
  $tasks['application']['verify'] = array(
    'title' => t('Verify'),
    'description' => t('Verify that the application is correctly configured.'),
    'provision_save' => TRUE,
    'access callback' => 'hosting_application_task_access',
    'dialog' => FALSE,
  );
  return $tasks;
}

/**
 * Access callback for tasks associated with applications.
 */
function hosting_application_task_access($node, $type) {
  //TODO: Implement some real checks here. @see hosting_task_menu_access().
  return TRUE;
}

/**
 * Menu wildcard loader callback; loads an application node.
 *
 * This is required in order to display tasks on application nodes.
 *
 * @param (int) $arg
 *   Node's numeric nid
 *
 * @return (bool|object)
 *   An application node object or FALSE.
 *
 * @see hosting_task_menu()
 */
function hosting_application_wildcard_load($arg) {
  if (!is_numeric($arg)) {
    return FALSE;
  }
  if ($node = node_load($arg)) {
    if ($node->type == 'application') {
      return $node;
    }
  }
  return FALSE;
}

/**
 * Implements hook_node_view().
 */
function hosting_application_node_view($node, $view_mode, $langcode = NULL) {
  if ($node->type == 'application') {
    if ($view_mode == 'full') {
      $node->content['info'] = array(
        '#prefix' => '<div id="hosting-platform-info" class="hosting-info-list">',
        '#suffix' => '</div>',
      );
      // Move all fields into the info box.
      foreach ($node->content as $key => $value) {
        if (substr($key, 0, 6) == 'field_') {
          $node->content['info'][$key]['#title'] = $value['#title'];
          $node->content['info'][$key]['#type'] = 'item';
          $node->content['info'][$key]['#markup'] = '';
          foreach ($value['#items'] as $item_key => $item) {
            $node->content['info'][$key]['#markup'] .= $value[$item_key]['#markup'] . "<br />";
          }
          unset($node->content[$key]);
        }
      }

      // Add the task list.
      if ($node->nid) {
        $node->content['tasks_view'] = array(
          '#type' => 'item',
          '#markup' => hosting_task_table($node),
          '#prefix' => '<div id="hosting-task-list">',
          '#suffix' => '</div>',
          '#weight' => 9,
        );
        $settings['hostingTaskRefresh'] = array(
          'nid' => $node->nid,
          'changed' => $node->changed,
        );
        drupal_add_js($settings, array('type' => 'setting', 'scope' => JS_DEFAULT));
        drupal_add_js(drupal_get_path('module', 'hosting_task') . '/hosting_task.js');
      }
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function hosting_application_node_insert($node) {
  if ($node->type == 'application') {
    // Always generate a new context.
    $hosting_name = isset($node->hosting_name) ? $node->hosting_name : 'application_' . preg_replace("/[!\W\.\-]/", "", $node->title);
    hosting_context_register($node->nid, $hosting_name);

    // TODO: determine whether something like hosting_server_invoke_services()
    // might be a useful way to handle resources.

    if (!isset($node->no_verify) || !$node->no_verify) {
      hosting_add_task($node->nid, 'verify');
    }
  }
}

/**
 * Implements hook_node_update().
 */
function hosting_application_node_update($node) {
  if ($node->type == 'application') {
    // If this is a new node or we're adding a new revision.
    if (!empty($node->revision)) {
      hosting_application_node_insert($node);
    }

    if ($node->field_application_status == 'deleted') {
      $node->no_verify = TRUE;
    }

    if (!isset($node->no_verify) || !$node->no_verify) {
      hosting_add_task($node->nid, 'verify');
    }
  }
}


/**
 * Implements hook_form_alter().
 *
 * Clean up the application form.
 */
function hosting_application_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'application_node_form') {
    $form['options']['#access'] = FALSE;
    $form['revision_information']['#access'] = FALSE;
    $form['author']['#access'] = FALSE;
    $form['actions']['preview']['#access'] = FALSE;

    // Remove delete button from application edit form,
    // unless the application's already been deleted via the Delete task.
// TODO: Add a 'delete' task, and set the appropriate status.
#    $node = $form['#node'];
#    if (isset($node->application_status) && $node->application_status !== HOSTING_APPLICATION_DELETED) {
      $form['actions']['delete']['#type'] = 'hidden';
#    }
    // Filter container servers to only those that provide a container service.
    $servers = $form['field_container_server']['und']['#options'];
    $container_servers = array();
    foreach ($servers as $nid => $title) {
      $node = node_load($nid);
      if (array_key_exists('container', $node->services)) {
        $container_servers[$nid] = $title . ' (' . $node->services['container']->name . ')';
      }
    }
    $form['field_container_server']['und']['#options'] = $container_servers;
  }
}

/**
 * Implements hook_menu().
 */
function hosting_application_menu() {
  $items = array();

  $items['hosting/applications/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['hosting/applications/add'] = array(
    'title' => 'Add application',
    'page callback' => 'drupal_goto',
    'page arguments' => array('node/add/application'),
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'node_access',
    'access arguments' => array('create', 'application'),
  );

  return $items;
}

/**
 * Implements hook_token_info().
 */
function hosting_application_token_info() {
  $info = array();
;
  $info['types'] = array(
    'application' => array(
      'name' => t('Application'),
      'description' => t('Application-related tokens.'),
    ),
  );

  $info['tokens'] = [
    'application' => [
      // [application:name]
      'name' => [
        'name' => t('Application name'),
        'description' => t('The name of the application.'),
        'needs-data' => array('node'),
      ],
      // [application:title]
      'title' => [
        'name' => t('Application title'),
        'description' => t('The title of the application.'),
        'needs-data' => array('node'),
      ],
    ],
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function hosting_application_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Replacement values for tokens that require additional contextual data.
  if ($type == 'application' && !empty($data['application'])) {
    $application = $data['application'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name':
          $machine_name = preg_replace('@[^a-z0-9-]+@','-', strtolower($application->title));
          $replacements[$original] = $machine_name;
          break;

        case 'title':
          $replacements[$original] = $application->title;
          break;
      }
    }
  }
  // An array of replacement values keyed by original token string.
  return $replacements;
}

