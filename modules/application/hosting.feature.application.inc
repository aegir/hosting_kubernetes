<?php

/**
 * @file
 * Expose the Aegir Application feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_application_hosting_feature() {
  $features['application'] = array(
    'title' => t('Application'),
    'description' => t('Provides an application content-type.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_application',
    #'enable' => 'hosting_application_feature_enable_callback',
    #'disable' => 'hosting_application_feature_disable_callback',
    // associate with a specific node type.
    'node' => 'application',
    'group' => 'experimental'
    );
  return $features;
}
