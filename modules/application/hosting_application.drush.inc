<?php

/**
 * @file
 * Implement drush hooks for the Application module.
 */

/**
 * Implements hook_hosting_TASK_OBJECT_context_options().
 */
function hosting_application_hosting_application_context_options(&$task) {
  $task->context_options['server'] = '@server_master';
  $container_server = field_get_items('node', $task->ref, 'field_container_server');
  $task->context_options['container_server'] = hosting_context_name($container_server[0]['target_id']);
  $task->context_options['resources'] = hosting_application_get_resources($task->ref);
}

/**
 * Return a list of resources appropriate for inclusion in an application
 * context.
 *
 * @todo move this to an application entity.
 */
function hosting_application_get_resources($entity, array &$files = [], int $parent_id = 0) {
  $entity_type = get_class($entity) == 'stdClass' ? 'node' : 'resource';
  $resource_fields = hosting_application_get_resource_entityreference_fields($entity);
  foreach ($resource_fields as $resource_field) {
    $entity_resources = field_get_items($entity_type, $entity, $resource_field);
    $ids = [];
    foreach ($entity_resources as $entity_reference) {
      $ids[] = $entity_reference['target_id'];
    }
    $resources = entity_load('resource', $ids);
    foreach ($ids as $id) {
      $resource = $resources[$id];
      // Recurse into resources that have resources of their own.
      if (count(hosting_application_get_resource_entityreference_fields($resource))) {
        hosting_application_get_resources($resource, $files, $id);
      }
      $files[$id] = [
        'filename' => $resource->getFilename(),
        'template' => $resource->getTokenizedTemplateYAML(0),
      ];
      if ($parent_id) {
        $files[$id]['parent_id'] = $parent_id;
      }
    }
  }
  return $files;
}

function hosting_application_get_resource_entityreference_fields($entity) {
  $entity_type = get_class($entity) == 'stdClass' ? 'node' : 'resource';
  $field_instances = field_info_instances($entity_type, $entity->type);
  $resource_entityreference_fields = [];
  foreach ($field_instances as $field => $instance) {
    $field_info = field_info_field($field);
    if ($field_info['type'] == 'entityreference') {
      if ($field_info['settings']['target_type'] == 'resource') {
        $resource_entityreference_fields[] = $field;
      }
    }
  }
  return $resource_entityreference_fields;
}

/**
 * Implements hook_drush_context_import().
 *
 * @todo Rework this to allow mapping of resources back to their respective
 * entityreferences.
 */
function hosting_application_drush_context_import($context, &$node) {
  if ($context->type == 'application') {
    if (!isset($node->title)) {
      $node->title = str_replace('application_', '', trim($context->name, '@'));
    }
    $node->field_container_server['und'][0]['target_id'] = hosting_context_nid($context->container_server);
    foreach ($context->resources as $key => $resource) {
      $node->field_application_resources['und'][$key]['target_id'] = $resource['id'];
    }
  }
}

