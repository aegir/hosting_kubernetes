<?php
/**
 * @file
 * hosting_application.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_application_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-application-field_application_resources'.
  $field_instances['node-application-field_application_resources'] = array(
    'bundle' => 'application',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_application_resources',
    'label' => 'Resources',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'type_settings' => array(
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'path' => '',
        ),
      ),
      'type' => 'inline_entity_form',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-application-field_application_status'.
  $field_instances['node-application-field_application_status'] = array(
    'bundle' => 'application',
    'default_value' => array(
      0 => array(
        'value' => 'queued',
      ),
    ),
    'deleted' => 0,
    'description' => 'The status of the application.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_application_status',
    'label' => 'Status',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-application-field_container_server'.
  $field_instances['node-application-field_container_server'] = array(
    'bundle' => 'application',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the server to which Aegir will deploy instances of this application.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_container_server',
    'label' => 'Container server',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Container server');
  t('Resources');
  t('Select the server to which Aegir will deploy instances of this application.');
  t('Status');
  t('The status of the application.');

  return $field_instances;
}
