<?php
/**
 * @file
 * Provides a ResourceEntityBundle class.
 */

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

/**
 * A quasi-Bundle class.
 *
 * EntityType and Bundle don't appear to be overrideable in Drupal 7.x. So we
 * define a basic class here that allows us to collect related methods and
 * properties, to make upgrading to Drupal 8.x.y easier.
 */
class ResourceEntityBundle {

  // The ECK entity bundle edit form array.
  public $form = FALSE;

  // The ECK entity bundle edit form state array.
  public $form_state = FALSE;

  // The ECK entity bundle config.
  public $config = FALSE;

  function __construct(&$form, &$form_state) {

    $this->form =& $form;
    $this->form_state =& $form_state;

    $entity_type_name = $form_state['build_info']['args'][0];
    $bundle_name = $form_state['build_info']['args'][1];
    $bundle = bundle_load($entity_type_name, $bundle_name);
    $this->config = $bundle->config;

  }

  /**
   * Callback for our implementation of hook_form_alter().
   *
   * @see hosting_resource_form_alter().
   */
  function alterEditForm() {

    $form_state =& $this->form_state;
    $entity_type_name = $form_state['build_info']['args'][0];

    if ($entity_type_name == 'resource') {

      $this->collapseManagedProperties();
      $this->addResourceTemplate();
      $this->addReplacements();
      $this->addTokenList();
      $this->addUpdateButton();
      $this->addEditFormValidator();
      $this->prependSubmitHandler();

    }
  }

  function addEditFormValidator() {

    $form =& $this->form;

    $form['#validate'][] = 'hosting_resource_eck_edit_form_validate';

  }

  function validateEditForm() {

    $form_state =& $this->form_state;

    $this->filterReplacements();

    $replacements =& $form_state['values']['config_replacements'];

    // Check that both elements of a replacement are present.
    foreach ($replacements as $delta => $replacement ) {

      $selector = isset($replacement['selector']) ? $replacement['selector'] : FALSE;
      $token = isset($replacement['token']) ? $replacement['token'] : FALSE;

      if (!$selector && !$token) {

        // Discard empty replacements.
        unset($replacements[$delta]);
        unset($form_state['replacement_deltas'][$delta]);

      }
      elseif (!$selector) {

        $error = t('Selector cannot be empty');
        form_set_error('config_replacements][' . $delta . '][selector', $error);

      }
      elseif (!$token) {

        $error = t('Token cannot be empty');
        form_set_error('config_replacements][' . $delta . '][token', $error);

      }

    }

    $template = Yaml::parse($this->getTemplate());

    foreach ($replacements as $delta => $replacement) {

      $parents = explode('/', $replacement['selector']);

      if (!drupal_array_get_nested_value($template, $parents)) {

        $error = t('Selector does not match any elements in the template.');
        form_set_error('config_replacements][' . $delta . '][selector', $error);

      }

    }

  }

  function prependSubmitHandler() {

    $form =& $this->form;

    $eck_submit = 'eck__bundle__edit_form_submit';
    $eck_submit_key = array_search($eck_submit, $form['#submit']);
    unset($form['#submit'][$eck_submit_key]);

    $form['#submit'][] = 'hosting_resource_eck_edit_form_submit';
    $form['#submit'][] = $eck_submit;

  }

  function submitEditForm() {

    // Clean up resource template data prior to ECK saving it.
    $this->filterTemplate();

    // Clean up replacement data prior to ECK saving it.
    $this->filterReplacements();

  }

  /**
   * Filter out form cruft (e.g., `update` button.)
   */
  function filterTemplate() {

    $form_state =& $this->form_state;

    $template = $form_state['values']['config_template']['resource'];

    $form_state['values']['config_template'] = ['resource' => $template];

  }

  /**
   * Filter out form cruft (e.g., `add` and `remove` buttons.)
   */
  function filterReplacements() {

    $form_state =& $this->form_state;

    $replacements = [];

    foreach ($form_state['values']['config_replacements'] as $key => $value ) {

      if (is_int($key)) {

        $replacements[$key] = [
          'selector' => $value['selector'],
          'token' => $value['token']
        ];

      }

    }

    $form_state['values']['config_replacements'] = $replacements;

  }

  /**
   * Collapse the `Managed properties` field.
   */
  function collapseManagedProperties() {

    $form =& $this->form;

    $form['managed_properties_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Managed properties'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $managed_properties = $form['config_managed_properties'];
    unset($form['config_managed_properties']);

    $form['managed_properties_fieldset']['config_managed_properties'] = $managed_properties;

  }

  /**
   * Add a resource file that will act as a template.
   */
  function addResourceTemplate() {

    $form =& $this->form;
    $form_state =& $this->form_state;
    $config =& $this->config;

    $form['config_template'] = [
      '#type' => 'fieldset',
      '#title' => t('Resource file template'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#prefix' => '<div id="resource-file-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    $form['config_template']['resource'] = [
      '#type' => 'hidden',
      '#default_value' => $this->getTemplate(),
    ];

    $form['config_template']['display'] = [
      '#markup' => $this->renderTemplate(),
    ];

    $form['config_template']['resource_file'] = [
      '#type' => 'file',
      '#name' => 'files[resource_file]',
    ];

    $resource = $form['config_template']['resource'];
    $template_exists = isset($resource['#default_value']) || isset($resource['#value']);
    $form['config_template']['upload'] = [
      '#type' => 'submit',
      '#value' => $template_exists ? t('Replace current template') : t('Upload new template'),
      '#submit' => [
        'hosting_resource_ajax_upload_submit',
      ],
      '#ajax' => [
        'callback' => 'hosting_resource_ajax_upload_callback',
        'wrapper' => 'resource-file-wrapper',
      ],
      '#name' => 'resource_file_upload',
    ];

  }

  /**
   * Add an arbitrary number of replacements.
   */
  function addReplacements() {

    $form =& $this->form;
    $form_state =& $this->form_state;

    $form['replacements'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacements'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['replacements']['config_replacements'] = [
      '#type' => 'container',
      '#weight' => 80,
      '#tree' => TRUE,
      '#prefix' => '<div id="replacements-wrapper">',
      '#suffix' => '</div>',
    ];

    $replacements = $deltas = [];
    foreach ($this->getReplacements() as $delta => $replacement) {
      $replacements[$delta] = $replacement;
      $deltas[$delta] = $delta;
    }

    $form_state['replacement_deltas'] = isset($form_state['replacement_deltas']) ? $form_state['replacement_deltas'] : $deltas;

    foreach ($form_state['replacement_deltas'] as $delta) {

      $form['replacements']['config_replacements'][$delta] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['container-inline'],
        ],
        '#tree' => TRUE,
      ];

      $form['replacements']['config_replacements'][$delta]['selector'] = [
        '#type' => 'textfield',
        '#title' => t('Selector'),
        '#default_value' => isset($replacements[$delta]) ? $replacements[$delta]['selector'] : NULL,
        '#size' => 20,
      ];

      $form['replacements']['config_replacements'][$delta]['token'] = [
        '#type' => 'textfield',
        '#title' => t('Token'),
        '#default_value' => isset($replacements[$delta]) ? $replacements[$delta]['token'] : NULL,
        '#size' => 20,
      ];

      $form['replacements']['config_replacements'][$delta]['remove'] = [
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#submit' => ['hosting_resource_ajax_replacement_remove'],
        '#ajax' => [
          'callback' => 'hosting_resource_ajax_replacement_callback',
          'wrapper' => 'replacements-wrapper',
        ],
        '#name' => 'remove_' . $delta,
      ];

    }

    $form['replacements']['config_replacements']['add'] = [
      '#type' => 'submit',
      '#value' => t('Add replacement'),
      '#submit' => ['hosting_resource_ajax_replacement_add'],
      '#ajax' => [
        'callback' => 'hosting_resource_ajax_replacement_callback',
        'wrapper' => 'replacements-wrapper',
      ],
      '#weight' => 100,
    ];

  }

  /**
   * Add an update button to run the validators.
   */
  function addUpdateButton() {

    $form =& $this->form;

    $form['actions']['update'] = [
      '#type' => 'button',
      '#value' => t('Update'),
    ];

  }

  /**
   * Add a list of available tokens.
   */
  function addTokenList() {

    $form =& $this->form;

    $form['replacements']['token_list'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['resource', 'application', 'deployment'],
      '#global_types' => FALSE,
      '#click_insert' => TRUE,
    ];
  }

  /**
   * Resource template file upload AJAX submit handler.
   *
   * @see hosting_resource_function ajax_upload_submit().
   */
  function submitUpload() {

    $form_state =& $this->form_state;

    $validators = ['file_validate_extensions' => ['yaml yml']];

    $file = file_save_upload('resource_file', $validators);

    if ($file) {

      $contents = $this->getFileContents($file);

      $errors = $this->ValidateYAML($contents);

      if (!empty($errors)) {
        foreach ($errors as $error) {
          form_set_error('config_template][resource_file', $error);
        }
      }
      else {
	// Store the temporary file in form_values so we can pass it back via
	// AJAX.
        $form_state['values']['config_template']['resource'] = $contents;
      }

    }

  }

  function getFileContents(stdClass $file) {

    return file_get_contents($file->uri);

  }

  /**
   * Ensure we're working with a single, valid YAML document.
   */
  function validateYAML(string $yaml) {

    $errors = [];

    try {
      Yaml::parse($yaml);
    }
    catch (Exception $e) {
      $errors[] = $e->getMessage();
    }

    return $errors;

  }

  /**
   * Resource template file upload AJAX callback.
   *
   * @see hosting_resource_function ajax_upload_callback().
   */
  function getUploadFormElements() {

    $form =& $this->form;

    $form['config_template']['resource']['#value'] = $this->getTemplate();
    $form['config_template']['upload']['#value'] = t('Replace current template');
    $form['config_template']['display']['#markup'] = $this->renderTemplate();

    return $form['config_template'];

  }

  /**
   * Replacement removal AJAX submit handler.
   *
   * @see hosting_resource_ajax_replacement_remove().
   */
  function removeReplacement() {

    $form_state =& $this->form_state;

    $delta_to_remove = $form_state['triggering_element']['#parents'][1];
    $delta = array_search($delta_to_remove, $form_state['replacement_deltas']);
    unset($form_state['replacement_deltas'][$delta]);

    $form_state['rebuild'] = TRUE;

    drupal_get_messages();

  }

  /**
   * Replacement addition AJAX submit handler.
   *
   * @see hosting_resource_ajax_replacement_add().
   */
  function addReplacement() {

    $form_state =& $this->form_state;

    $form_state['replacement_deltas'][] = count($form_state['replacement_deltas']) > 0 ? max($form_state['replacement_deltas']) + 1 : 0;

    $form_state['rebuild'] = TRUE;

    drupal_get_messages();

  }

  /**
   * Replacement addition/removal AJAX callback.
   *
   * @see hosting_resource_ajax_replacement_callback().
   */
  function getReplacementFormElements() {

    $form =& $this->form;

    return $form['replacements']['config_replacements'];

  }

  function formatTemplate (string $template) {

    $template = trim($template, '---\n');

    return '<pre><code>' . $template . '</code></pre>';

  }

  function getReplacements() {

    if (isset($this->form_state['values'])) {

      $this->filterReplacements();

      return $this->form_state['values']['config_replacements'];

    }

    elseif (isset($this->config['replacements'])) {

      return $this->config['replacements'];

    }

    else {

      return [];

    }

  }

  function getTemplate() {

    if (isset($this->form_state['values'])) {

      $this->filterTemplate();

      return $this->form_state['values']['config_template']['resource'];

    }
    elseif (isset($this->config['template'])) {

      return $this->config['template']['resource'];

    }
    else {

      return FALSE;

    }

  }

  function renderTemplate() {

    if ($template = $this->getTemplate()) {

      $replacements = $this->getReplacements();

      $tokenized = $this->tokenize($template, $replacements);

      return $this->formatTemplate($tokenized);

    }

  }

  function tokenize(string $template, array $replacements) {

    $template = Yaml::parse($template);

    foreach ($replacements as $delta => $replacement) {

      $parents = explode('/', $replacement['selector']);

      if (drupal_array_get_nested_value($template, $parents)) {

        drupal_array_set_nested_value($template, $parents, $replacement['token']);

      }

    }

    ResourceEntityBundle::hardcodeMetadata($template);

    return Yaml::dump($template, 100);

  }

  function hardcodeMetadata(array &$template) {

    $template['metadata']['name'] = '[deployment:name][resource:parent_id]';
    $template['metadata']['labels']['application'] = '[deployment:title]';

  }

}
