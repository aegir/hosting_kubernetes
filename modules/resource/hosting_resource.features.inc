<?php
/**
 * @file
 * hosting_resource.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hosting_resource_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_entity_type_info().
 */
function hosting_resource_eck_entity_type_info() {
  $items = array(
    'resource' => array(
      'name' => 'resource',
      'label' => 'Resource',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'title' => array(
          'label' => 'Name',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
  );
  return $items;
}
