<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

/**
 * A class to collect common resource config file functionality.
 */
class Provision_Config_Resource extends Provision_Config {

  public $template = FALSE;

  /**
   * Add leading zeroes to ensure proper order of resource file templates.
   */
  function padKey($key) {
    $key_magnitude = $key == 0 ? 0 : floor(log10($key));
    $count_magnitude = floor(log10(count($this->getResources())));
    $padding = $count_magnitude - $key_magnitude;
    if ($padding) {
      for ($i = 0; $i < $padding; $i++) {
        $key = '0' . $key;
      }
    }
    return $key;
  }

  function unpackYAML(string $yaml) {
    return Yaml::dump(Yaml::parse($yaml), 100);
  }

  function getResources() {
    return $this->getContext()->resources;
  }

  function getContext($type) {
    $context = d();
    if ($context->type == $type) {
      return $context;
    }
    elseif (isset($context->$type)) {
      return $context->$type;
    }
    else {
      drush_log(dt('Unrecognized context type in getApplication(): :type.', array(':type' => $context->type)), 'error');
    }
  }

  function getCleanName() {
    $context = $this->getContext();
    $prefix = '@' . $context->type. '_';
    return substr($context->name, strlen($prefix));
  }

  function getContainerServer() {
    return $this->getContext('application')->container_server;
  }

  function getConfigPath($key, $filename) {
    $padded_key = $this->padKey($key);
    return $this->getResourceDir() . $padded_key . '-' . $filename;
  }

  function getResourceDir() {
    $dir = $this->getResourcesDir() . $this->getCleanName() . '/';
    drush_mkdir($dir);
    return $dir;
  }

  function getResourcesDir() {
    // @todo change this to `resource_config_path`.
    $dir = $this->getContainerServer()->kubernetes_config_path . '/' . $this->getContext()->type . 's/';
    drush_mkdir($dir);
    return $dir;
  }

}
