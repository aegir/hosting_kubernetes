<?php
/**
 * @file
 * Code for the Aegir Container Resource feature.
 */

include_once 'hosting_resource.features.inc';
include_once 'hosting_resource.form.inc';

/**
 * Implements hook_entity_info_alter().
 */
function hosting_resource_entity_info_alter(&$info) {
  // Override the entity and controller classes with our own, so that we can
  // extend them as needed.
  $info['resource']['entity class'] = 'ResourceEntity';
  $info['resource']['controller class'] = 'ResourceEntityController';
}

/**
 * Implements hook_token_info_alter().
 */
function hosting_resource_token_info_alter(&$data) {
  // Remove extraneous tokens.
  unset($data['tokens']['resource']['uid']);
  unset($data['tokens']['resource']['created']);
  unset($data['tokens']['resource']['changed']);

  $data['tokens']['resource']['parent_id'] = [
    // [resource:parent_id]
    // N.B. Must be passed into token_generate() via the $options parameter,
    // keyed to 'parent_id'.
    // @see: hosting_deployment_get_replacements().
    'name' => t('Resource parent ID.'),
    'description' => t('The resource ID for the parent resource.'),
  ];

  $data['tokens']['resource']['type'] = [
    // [resource:type]
    'name' => t('Resource type.'),
    'description' => t('The resource bundle name.'),
    'needs-data' => array('resource'),
  ];

}

/**
 * Implements hook_tokens().
 */
function hosting_resource_tokens_alter(array &$replacements, array $context) {
  $type = $context['type'];
  $tokens = $context['tokens'];
  $data = $context['data'];
  $options = $context['options'];

  if ($type == 'resource') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'parent_id':
          $replacements[$original] = isset($options['parent_id']) ? $options['parent_id'] : '';
          break;
        case 'type':
          $replacements[$original] = str_replace('_', '-', $data['resource']->type);
          break;
      }
    }
  }

  return $replacements;
}

