Aegir Kubernetes Support
========================

This module brings [Kubernetes](http://kubernetes.io) support to the [Aegir Hosting System](http://docs.aegirproject.org)

It is in the very early stages of development and, as such, is not yet fully functional, nor well-tested.

Members of the [Ægir Cooperative](http://aegir/coop) are developing it on behalf of the [National Democratic Institute](http://www.ndi.org), who have graciously funded this early phase of development.


Dependencies
------------

This module has a number of dependencies. Most notable, of course, is Aegir itself. The module ships with a Drush makefile that documents the dependencies that are not already included with Aegir.

The makefile should be recursively built if this module is included in an Aegir platform makefile. Otherwise, the dependencies can be installed using the following commands:

    $ cd /var/aegir/hostmaster-7.x-3.x
    $ drush make /path/to/hosting_kubernetes/hosting_kubernetes.make.yml --no-core -y


Installation
------------

This module is installed as you normally would with a Drupal module. That is, download it to (for example) `sites/all/modules` and enable it `admin/modules`. Alternatively, being a Hosting Feature, you can also enable it via `admin/hosting`. On the command-line, `drush @hm em hosting_kubernetes` will also work.

Now, when you edit a server node, the "Container" service type will be available, along with the "Kubernetes" implementation.

Deploying Kubernetes itself is beyond the scope of this module. Just as we do with Aegir's web or database servers, we assume that that a Kubernetes cluster has already been deployed, and that `kubectl` is available on the server. The server must also have an `aegir` user that can be SSH'd into from the master server, and that has permission to run `kubectl` commands.

For development purposes, this is done via a combination of GNU Makefiles and Ansible. These can serve as useful guides. In the future, we may publish full-fledged Ansible roles to deploy a complete system.


Development
-----------

See: CONTRIBUTING.md


Usage
-----

See: USAGE.md

