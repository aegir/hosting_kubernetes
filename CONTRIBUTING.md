Aegir Kubernetes Support
========================

See README.md for information on dependencies and installation. See USAGE.md for information on using the feature of this suite of modules.

Development
-----------

Both GNU/Linux and OSX are supported, but require:

* [Virtualbox 5.1+](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant 1.8.5+](https://www.vagrantup.com/downloads.html)


### Getting started

To get started developing this module, bootstrap [Drumkit](http://drumk.it/), which will download and install `minikube` and `kubectl`:

    $ git submodule update --init
    $ . d
    [...]
    minikube version: v0.10.0

Then, we need to start the local Kubernetes cluster, along with a local Aegir server:

    $ make kube-start
    [...]

To confirm that things are running as expected, run:

    $ make kube-status
    minikubeVM: Running
    localkube: Running
    Aegir: running

You'll need to add an entry to your `/etc/hosts` along the lines of:

    192.168.99.200   aegir.local

To login to the locally-installed Aegir UI and Kubernetes dashboards, run:

    $ make kube-dashboards

For additional options, run:

    $ make kube-help
    The following 'make' targets are available: (usage 'make <target>')
      kube-help
        Print this help message.
      kube-status
        Print a brief status of the various VMs.
      kube-dashboards
        Open both Kubernetes and Aegir dashboard in a browser.
      kube-start
        Launch VMs and run initial configuration.
      kube-stop
        Stop the currently running VMs.
      kube-destroy
        Print this help message
      kube-rebuild
        Print this help message


### See it in action

After enabling the module (see "Installation", above), you can add a server named "minikube". An entry was added in `/etc/hosts` that will resolve to the minikubeVM. Choose the Kubernetes Container service, and save the node. After the "verify" task has run, you should see the following in the task log:

    Kubernetes is accessible.
    [...]
    Kubernetes configuration path /var/aegir/config/server_minikube/kubernetes.d exists.
    Kubernetes configuration ownership of /var/aegir/config/server_minikube/kubernetes.d has been changed to aegir.
    Kubernetes configuration permissions of /var/aegir/config/server_minikube/kubernetes.d have been changed to 700.
    Kubernetes configuration path /var/aegir/config/server_minikube/kubernetes.d is writable.


Next Steps
----------

* ~~Add the a "Container" Hosting Service type.~~ **done**
* ~~Add a "Kubernetes" implementation of the "Container" service.~~ **done**
* ~~Ensure that `kubectl` is installed and runnable by `aegir` on a server `verify` task.~~
    * ~~Create `aegir` user on minikube VM.~~ **done**
    * ~~Add ssh pub key from aegir server to minikube `aegir`'s authorized_keys.~~ **done**
    * ~~Connect once as `aegir` via SSH to accept host key.~~ **done**
    * ~~Install kubectl on minikube.~~ **done**
    * ~~Add verify hook.~~ **done**
* Implement deployment/application status via container/kubernetes status() method.
    * kubernetes implementation will query the kubernetes API.
    * On server page as well
    * List master cluster nodes.
    * Need deployment IP address.
* Add list of deployments to application nodes.
* Switch deployment status to vocabulary.
* ~~Entity type and bundles for Resource.~~ **done**
* ~~Filter server list on applications to only show servers that implement container service.~~ **done**
* Add additional tasks:
    * ~~delete deployments~~ **done**
    * delete applications
    * clone deployments
    * backup & restore deployments
    * update deployments:
        * push app data into deployment.  can then update app before intentionally updating deployment.
    * update applications:
        * schedule updates of all dependant deployments
* Validate resources (remote or local files) on node creation. (?)
* ~~Add Application button on applications list.~~ **done**
* ~~Add Deployment button on deployments list.~~ **done**
* ~~Add Deployment button to application nodes.~~ **done**

