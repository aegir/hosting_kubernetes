<?php

/**
 * @file
 * Expose the Kubernetes feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_kubernetes_hosting_feature() {
  $features['kubernetes'] = array(
    'title' => t('Kubernetes Service'),
    'description' => t('Provides integration between the Aegir Hosting System and Kubernetes.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_kubernetes',
    'enable' => 'hosting_kubernetes_feature_enable_callback',
    'disable' => 'hosting_kubernetes_feature_disable_callback',
    // associate with a specific node type.
    //  'node' => 'nodetype',
    'group' => 'experimental'
    );
  return $features;
}
