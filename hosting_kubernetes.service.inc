<?php

/**
 * @file
 * Kubernetes service implementation of the container service type for the
 * hosting front end.
 */

/**
 * An implementation of the container service type, registered with
 * hook_hosting_service().
 */
class hostingService_container_kubernetes extends hostingService_container {
  /**
   * the value stored in the type column of the hosting_service table.
   */
  public $type = 'kubernetes';

  /**
   * The name displayed to users when creating or editing a server.
   */
  public $name = 'Kubernetes';

  /**
   * This service needs to have a port specified for it.
   */
  public $has_port = TRUE;

  /**
   * The default value for the port input.
   */
  function default_port() {
    return 12345;
  }

  /**
   * This service needs to be restarted with a shell command.
   */
  public $has_restart_cmd = FALSE;

  /**
   * Provides the default path to the kubectl binary.
   */
  function default_kubectl_path() {
    return '/usr/local/bin/kubectl';
  }

  /**
   * node operations
   */

  /**
   * Load associated values for the service.
   */
  function load() {
    parent::load();
    $this->kubectl_path = variable_get('hosting_kubectl_path_' . $this->server->nid, $this->default_kubectl_path());

    /**
     * Although this container does not have it's own tables, we provide some
     * utitilty functions for use in this method.
     *
     * If this container used it's own tables, we could use the mergeData
     * method below to merge in the results automatically, instead of iterating
     * through the results ourself.
     */

    // $this->mergeData("SELECT kubectl_path FROM {hosting_container} WHERE vid = :vid", array(':vid' => $this->server->vid));
  }

  /**
   * Display settings on the server node page.
   *
   * Modify the reference passed to the method to add additional implementation
   * specific fields to be displayed.
   *
   * @param
   *   A reference to the associative array of the subsection of the page
   *   reserved for this service implementation.
   */
  function view(&$render) {
    parent::view($render);
    $render['kubectl_path'] = array(
      '#type' => 'item',
      '#title' => t('container field'),
      // Remember to pass the display through filter_xss!
      '#markup' => filter_xss($this->kubectl_path),
    );
  }

  /**
   * Extend the server node form.
   *
   * Modify the reference passed to the method to add additional implementation
   * specific fields to be stored and managed.
   *
   * @param
   *   A reference to the associative array of the subsection of the form
   *   reserved for this service implementation.
   */
  function form(&$form) {
    parent::form($form);

    $form['kubectl_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to kubectl'),
      '#description' => t('The path to the kubectl binary.'),
      '#size' => 40,
      '#default_value' => isset($this->kubectl_path) ? $this->kubectl_path : $this->default_kubectl_path(),
      '#maxlength' => 64,
      '#weight' => 5,
    );
  }

  /**
   * Validate a form submission.
   */
  function validate(&$node, &$form, &$form_state) {
    parent::validate($node, $form, $form_state);

    // Validate that we've entered a valid system path.
    $path = trim($this->kubectl_path);
    if (preg_match('/^[^*?"<>|:]*$/',$path)) {
      return true;
    }
    else {
      form_set_error('kubectl_path', t("The kubectl path must be a valid system path."));
    }
  }

  /**
   * Insert a record into the database.
   *
   * Called by hosting_server_hook_insert().
   *
   * The values associated with this implementation have already
   * been set as properties of $this object, so we now need to
   * save them.
   *
   * For this container we will use the variables table, but you should
   * create your own tables with an install file and hook_schema.
   */
  function insert() {
    parent::insert();

    variable_set('hosting_kubectl_path_' . $this->server->nid, $this->kubectl_path);
  }

  /**
   * Update a record in the database.
   *
   * Called by hosting_server_hook_update().
   *
   * For this container we will use the variables table, but you should
   * create your own tables with an install file and hook_schema.
   */
  function update() {
    parent::update();

    variable_set('hosting_kubectl_path_' . $this->server->nid, $this->kubectl_path);
  }

  /**
   * Delete a record from the database, based on server node.
   */
  function delete() {
    parent::delete();

    variable_del('hosting_kubectl_path_' . $this->server->nid);
  }

  /**
   * Delete a specific reivision from the database.
   *
   * Not relevant in our container but shown anyway.
   */
  function delete_revision() {
    parent::delete_revision();
  }

  /**
   * Pass values to the provision backend when we call provision-save.
   *
   * By selecting this type we already pass the '--container_service_type=kubernetes' option
   * to the command, which will load the matching provisionService class in the backend.
   *
   * This backend class will be responsible for receiving and reacting to the options
   * passed here.
   *
   * @ingroup backend-frontend-IPC
   */
  public function context_options($task_type, $ref_type, &$task) {
    parent::context_options($task_type, $ref_type, $task);

    $task->context_options['kubectl_path'] = $this->kubectl_path;
  }
}
