<?php

/**
 * @file
 *   Add a Kubernetes service to the provision service API.
 */

/**
 * Implements hook_drush_init().
 */
function kubernetes_drush_init() {
  kubernetes_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function kubernetes_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

