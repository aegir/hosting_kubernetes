Aegir Kubernetes Support
========================

Aegir Kubernetes ships with a suite of example resources that, when combined, will deploy a basic WordPress site to Kubernetes. These are used to illustrate functionality throughout the instructions below. However, Aegir Kubernetes was designed to be (relatively) easily extended, and should support pretty much all of Kubernetes' functionality. See the `Developer` section below for an in-depth discussion.


Usage
-----

### End-user

Once an administrator has created a Kubernetes server and one or more applications, deploying such an application is simple. For illustrative purposes, we'll deploy a WordPress site.

First, click on the "Applications" menu, then select "WordPress" from the list. Next, click on the "Add deployment" tab, whcih will bring you to the "Create Deployment" form. Provide a name for your deployment (e.g., "www.example.com"). The correct application should already have been selected, so you should be able to simple click the "Save" button to deploy the application.

With the creation of a "Deployment" node, a "Deploy-Application" task will have been added to the queue. This will create various Kubenetes configuration files representing the various resources required to deploy your application. It will then tell Kubenetes to create these resources by running the `kubectl create` command on the Kubernetes server, and passing it each of the configuration files in turn.

This will result in the creation of two "volumes" where the database's and WordPress' files will be stored, along with containers running the MySQL and Apache services. In addition, some resources to help tie the components together will be created. These include a shared secretthat contains the databases root password, along with routing to allow the containers to find each other reliably, as well as load-balance web traffic across Apache containers.

If this is the very first such deployment, images for the various containers will need to be downloaded. This may result in timeouts that prevent the WordPress site from installing properly. If this appears to be the case, simply run a "Delete" task on your application, followed by a "Deploy" task. With the images already downloaded, the site should be up and running very quickly.

For development, using the local minikube VM, DNS won't provide viable routes to your new site. To access it, you'll need to access your Kubernetes dashboard (`minikube dashboard`). Click on the "Services" menu item, which will present a list of running routing services. The one we're looking for is likely stuck in a "pending" state, as it isn't going to be able to bind to an external IP address. It should work regardless, using the internal port number.

Under the "Internal endpoints" column, find the port number (hint: it'll be above `30000`). Open a new browser tab, and enter the IP of the minikube server (kint: it'll be something like `192.168.99.10x`), followed by the port number (e.g., `http://192.168.99.100:31234`). This should now bring you to the WordPress installation screen.

In the future, we'll provide a link from within the Aegir UI directly to the deployment, as we already do with Drupal sites.


### Administrator

A preliminary step in setting up a Kubernetes-based application is to ensure that a Kubernetes server exists and is registered with Aegir. This step is documented in the "Installation" section of the README.

Once a developer has defined the appropriate resources, creating an application is fairly straight-forward. For illustrative purposes, we'll create a WordPress application.

We'll first need to enable the "WordPress Deployment" feature (`/admin/structure/features`) under the "Kubernetes Resources" section. Since this resource depends on the rest, all of the default resources ough to be enabled as a result.

Next, we need to add an application. Click on the `Applications` menu item, then the `Add application` tab. Give the application a name (e.g., "WordPress"), and select a container server (e.g., "minikube"). From the `Resources` drop-down, select "WordPress Deployment" and click the "Add new Resource" button.

The form should now expand to present various options. These will include the size of the volumes that will be provisioned for storage of MySQL database files and WordPress' codebase and uploaded files, as well as the container images to use for deploying MySQL and WordPress. Reasonable defaults are provided, so next click "Create Resource".

The inline form will collapse down to a single line representing the resource we just defined. If you need to make changes or remove it, buttons are provided for such operations on a resource-by-resource basis. You could also now add additional resources. However, because resources can themselves contain other resources, this should be all that is required (and recommended) for a WordPress application.

Simply click "save" and the application node will be created. This will also trigger a "Verify" task that will create some configuration files for the application, and sync them out to the Kubernetes server.

If you are curious about these, you can explore the Drush alias (`/var/aegir/.drush/application_WordPress.alias.drushrc.php`), which contains a "resources" array that defines all the resource templates that were, in turn, created in `/var/aegir/config/server_minikube/kubernetes.d/applications/WordPress/`.

### Developer

Defining resources that combine easily to form an application can be somewhat tricky. We'll explore, in some depth, how the default resources combine in layers to provide a fully functional WordPress deployment.
